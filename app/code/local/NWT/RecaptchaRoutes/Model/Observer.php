<?php
class NWT_RecaptchaRoutes_Model_Observer
{
   /**
   * Add the route for the form to the available selections in configuration.
   *
   * @param Varien_Event_Observer $observer The dispatched observer
   */
  public function addRecaptchaRouteToConfiguration(Varien_Event_Observer $observer)
  {
    $observer->getRoutes()->add('newsletter_subscriber_new', 'Footer Newsletter Form');
  }

  public function onFailedRecaptcha(Varien_Event_Observer $observer)
  {
    /** @var Mage_Core_Controller_Front_Action $controller */
    $controller = $observer->getEvent()->getControllerAction();
    /** @var Studioforty9_Recaptcha_Helper_Response $response */
    $response = $observer->getEvent()->getRecaptchaResponse();
    if($errorResponse = $response->getErrors()) {
      Mage::log('google error response', null, 'grecaptcha.log');
      Mage::log($errorResponse, null, 'grecaptcha.log');
    }
  }
  
  public function onFailedRecaptchaRouteControllerAction(Varien_Event_Observer $observer)
  {
    $data = $observer->getEvent()->getControllerAction()->getRequest()->getPost();
    Mage::getSingleton('core/session')->setFormData($data);
  }
}

php /home/vanbruun/htdocs/custom/idex.php full -- $@

ERR=$?

if [ $ERR -eq 0 ]; then
	echo "error"
	exit 3 #exit shell script
fi

php /home/vanbruun/script/clearalldiamonds.php

php /home/vanbruun/magmi/cli/magmi.cli.php -profile=ClearDiamonds -mode=update

php /home/vanbruun/magmi/cli/magmi.cli.php -profile=live -mode=create

php /home/vanbruun/htdocs/shell/indexer.php --reindex catalog_product_price

php /home/vanbruun/htdocs/shell/indexer.php --reindex catalog_category_product

php /home/vanbruun/htdocs/shell/indexer.php --reindex catalog_product_attribute

/usr/bin/php /home/vanbruun/htdocs/shell/solrbridge.php -reindex vanbruunen

find /home/vanbruun/htdocs/custom/imported/csv -name 'update_*' -type f -mtime +4 -exec rm {} \;

find /home/vanbruun/htdocs/custom/imported/xml -name 'Full.*' -type f -mtime +4 -exec rm {} \;

find /home/vanbruun -name 'Full*' -type f -exec mv -t /home/vanbruun/imported {} \;

find /home/vanbruun/imported -name 'Full*' -type f -mtime +4 -exec rm {} \;

php /home/vanbruun/htdocs/shell/indexer.php --reindex catalog_url

echo "All done"
